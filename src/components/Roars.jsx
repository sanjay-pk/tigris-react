function Roars(props){
    return(
        <div>
            <article className="blog">
              <img
                src={props.items.image}
                alt="images of tiger"/>
              <h3>{props.items.title}</h3>
              <p>{props.items.para}</p>
            </article>
         </div>
    )
}
export default Roars