import './App.css'
import Roars from './components/Roars'
import Footer from './components/footer'
import Header from './components/header'

function App() {

    const datas = [{
      id : 1,
      image : "https://plus.unsplash.com/premium_photo-1669725687150-15c603ac6a73?q=80&w=1374&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
      title :"Exploring Tiger Behavior: Insights from the Wild" ,
      para : "Discover the enigmatic world of tiger behavior in its natural habitat. Our blog offers captivating insights into the daily lives and fascinating behaviors of these majestic creatures."
    },
    {
      id : 2,
      image : "https://images.unsplash.com/photo-1591824438708-ce405f36ba3d?q=80&w=1374&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
      title :"Into the Jungle: Tales from TigerScape's Expeditions" ,
      para : "Join us as we venture deep into the jungle, sharing captivating tales of discovery, adventure, and conservation efforts within TigerScape's vast wilderness."
    },
    {
      id : 3,
      image : "https://images.unsplash.com/photo-1598214325784-4b28e27e64e9?q=80&w=1374&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
      title :"Tiger Tales: Stories of Survival and Resilience" ,
      para : " Tiger Tales shares narratives of survival and resilience, unveiling the triumphs and challenges faced by these magnificent creatures in their ever-changing habitats."
    },
    {
      id : 4,
      image : "https://plus.unsplash.com/premium_photo-1664304262892-e4b05b1d4951?q=80&w=1374&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
      title :"Conservation: Saving Tigers, One Story at a Time" ,
      para : " Follow our journey through the Conservation Chronicles, where we document the tireless efforts and inspiring stories behind saving tigers, one step at a time."
    },
    {
      id : 5,
      image : "https://images.unsplash.com/photo-1616869736810-a674a04d0440?q=80&w=1374&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
      title :"Wildlife Photography: Capturing Tigers in Their Element" ,
      para : "Explore the artistry of wildlife photography as we showcase stunning images capturing tigers in their natural habitats, celebrating their beauty and grace."
    },
    {
      id : 6,
      image : "https://images.unsplash.com/photo-1587219408895-24918d2b78af?q=80&w=1376&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
      title :"Tiger Spotting: Spotting Encounters and Observations" ,
      para : "Dive into the thrill of Tiger Spotting, where we recount encounters and observations, capturing the essence of these elusive predators in their natural environment."
    }
  ]

    const Articles = datas.map(item=>
      <Roars key={item.id} items = {item}/>
    )
  
  return (
  <div>
  <Header/>
  <main>
    <section>
      <div className="container">
        <h1>The Legacy of Tigers in the Wild</h1>
        <p>
          Welcome to Tigerscape, where nature's magnificence meets the raw power
          of the jungle's rulers. Nestled within lush wilderness, Tigerscape
          Reserve offers a sanctuary for these majestic creatures. Traverse
          through dense forests, breathe in the untamed air, and witness the
          awe-inspiring beauty of tigers roaming freely. Embark on an
          unforgettable journey into the heart of Tigerscape.
        </p>
        <div className="buttons">
          <a href="#" className="button button-primary">
            Book Your Safari{" "}
            <span className="material-symbols-outlined">arrow_forward</span>
          </a>
          <a href="#" className="button button-secondary">
            Contact Us
          </a>
        </div>
      </div>
    </section>
    <section id="section1">
      <div className="container">
        <h2>Our Mission: Protecting Tigers and Preserving Their Habitat</h2>
        <p>
          TigerScape stands as a beacon of hope for the preservation of tigers
          and their habitat. Our unwavering commitment to conservation drives us
          to implement innovative strategies, advocate for wildlife protection,
          and engage communities in sustainable practices. Through research,
          education, and on-the-ground initiatives, we strive to secure a future
          where tigers roam freely and ecosystems flourish. Join us in
          safeguarding the legacy of these majestic creatures for generations to
          come.
        </p>
      </div>
    </section>
    <section classs="section2">
        <div className="container">
          <h2 id="blogHead">Roars and Insights</h2>
          <div className="blogList">
            {Articles}
          </div>
        </div>
    </section>
  </main>
  <Footer/>
</div>

  )
}

export default App
